INTRODUCTION
--------

Roundabout is a module for creating slideshows and carousel-like animations
with help of a 3D Turntable jQuery Roundabout plugin. It easily converts
unordered lists & other nested HTML structures into entertaining, interactive,
turntable-like areas. This module currently works with views.

This project is in an early stage and doesn't support all options the js
library provides. These will be added over time.

FEATURES
--------

Reflect
Autoplay on/off
Autoplay Initial Delay
Autoplay duration
Pause on hover on/off
Duration
jQuery Easing
See the Roundabout pages for documentation on these settings.

REQUIREMENTS
------------

This module requires the following modules:

 * Views (https://drupal.org/project/views)
 * Libraries (https://www.drupal.org/project/libraries)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.

FUTURE ENHANCEMENTS
-------------------

Integration with fields.
Implement remaining options for roundabout.
Responsive layout.
Event.drag and Event.drop support.
Drush command line installation.

MAINTAINERS
-----------

Current maintainers:
 * ASHISH PANDEY (ashishdeynap) - https://drupal.org/user/3417767
